# Addressmaster base image

Base image to build [Addressmaster](https://gitlab.com/unidata-community/addressmaster) container.
Intended for the speedup that process and use [jib gradle plugin](https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin) to do not require docker image presence.