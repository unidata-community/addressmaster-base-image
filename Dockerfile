FROM tomcat:9-jre11-slim

LABEL maintainer='Pavel Alexeev <Pavel.Alekseev@TaskData.com>'

RUN rm -rf /usr/local/tomcat/webapps/*
# Workaround of 'update-alternatives: error: error creating symbolic link '/usr/share/man/man7/ABORT.7.gz.dpkg-tmp': No such file or directory' ( https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=866729 ) on installation of postgresql-client
RUN mkdir -p /usr/share/man/man7/
RUN apt-get update -y -q \
    && apt-get install -y --no-install-recommends \
        curl \
        pgdbf postgresql-client \
        pv \
            && rm -rf /var/lib/apt/lists/
